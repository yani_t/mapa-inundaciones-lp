# Mapa de entrevistas :boy: :girl: :woman: :man: :older_woman: :older_man:

El mapa de entrevistas es un mapa digital accesible el cual permita gestionar entrevistas testimoniales acerca de la inundación de La Plata ocurrida en el año 2013 geolocalizadas por zona inundable.

## :wrench: Instalación

### :pushpin: Clonación del proyecto:

Se puede realizar clonando el repositorio a traves de:
    git clone https://gitlab.com/yani_t/mapa-inundaciones-lp.git 

### :pushpin: ¡Importante!

Este proyecto se encuentra **dockerizado** por lo que al crear la imagen de Docker este contiene todo lo necesario para que la aplicación se ejecute sin problemas en cualquier entorno de contenedor de Docker, independientemente del sistema operativo o la configuración del host en el que se ejecuta. 

### :pushpin: Pasos para levantar el proyecto: 

1. Contruir la imagen de Docker a traves del comando: 
    docker build -t mapa-entrevistas .

2. Una vez construida se debe iniciar el contenedor o imagen de Docker creada anteriormente con:
    docker run -p 8080:80 mapa-entrevistas

3. Con el comando anterior se ejecutará la imagen de Docker y se expondrá el sitio a través del puerto 8080 de la maquina host. Por lo que, al abrir un navegador web y acceder a la dirección http://localhost:8080 podremos ver el proyecto corriendo.

## :camera: Imágenes del proyecto en ejecución

![Mapa general](/img/capturas/mapa1.webp "Mapa de entrevistados")

![Detalle de una entrevista](/img/capturas/entrevista-detalle.webp "Entrevista a Suarez Amina")
