//Informacion de la zona en el momento de la inundación en ESPAÑOL
let barrioJardin = "<h1>Situación al 2 de abril del 2013</h1>"+
"<p>Esta zona registró en algunos sectores un nivel de severidad Elevada.</p>"+
"<p>Esto significa un riesgo alto para las personas en el exterior.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 2 metros/segundo.</p>"+
"<p>El nivel máximo de profundidad de inundación fue de 1.06 a 1.65 metros.</p>";
let villaElvira = "<h1>Situación al 2 de abril del 2013</h1>"+
"<p>Esta zona registró en algunos sectores un nivel de severidad Extrema.</p>"+
"<p>Esto significa que puede llegar a presentarse daños a edificios.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 3 m/s. </p>"+
"<p>El nivel máximo de profundidad de inundación fue mayor a 1.65 metros.</p>";
let altosDeSanLorenzo = "<h1>Situación al 2 de abril del 2013</h1>"+
"<p>Esta zona registró en algunos sectores un nivel de severidad Extrema.</p>"+
"<p>Esto significa que puede llegar a presentarse daños a edificios.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 3 m/s. </p>"+
"<p>El nivel máximo de profundidad de inundación fue mayor a 1.65 metros.</p>";
let parqueCasteli = "<h1>Situación al 2 de abril del 2013</h1>"+
"<p>Esta zona registró en algunos sectores un nivel de severidad Extrema.</p>"+
"<p>Esto significa que puede llegar a presentarse daños a edificios.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 3 m/s. </p>"+
"<p>El nivel máximo de profundidad de inundación fue de 1.06 a 1.65 metros.</p>";
let LosHornos = "<h1>Situación al 2 de abril del 2013</h1>"+
"<p>Esta zona registró en algunos sectores un nivel de severidad Extrema.</p>"+
"<p>Esto significa que puede llegar a presentarse daños a edificios.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 3 m/s.</p>"+
"<p>El nivel máximo de profundidad de inundación fue mayor a 1.65 metros.</p>";
let Tolosa = LosHornos;
let estadioMaradona = "<h1>Situación al 2 de abril del 2013</h1>"+
"<p>Esta zona registró en algunos sectores un nivel de severidad Elevada.</p>"+
"<p>Esto significa un riesgo alto para las personas en el exterior.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 3 m/s.</p>"+
"<p>El nivel máximo de profundidad de inundación fue mayor a 1.65 metros.</p>";
let barrioNorte = "<h1>Situación al 2 de abril del 2013</h1>"+
"<p>Esta zona registró en algunos sectores un nivel de severidad Extrema.</p>"+
"<p>Esto significa que puede llegar a presentarse daños a edificios.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 3 m/s.</p>"+
"<p>El nivel máximo de profundidad de inundación fue de 1.06 a 1.65 metros.</p>";
let Ringuelet = "<h1>Situación al 2 de abril del 2013</h1>"+
"<p>Esta zona registró en algunos sectores un nivel de severidad Extrema.</p>"+
"<p>Esto significa que puede llegar a presentarse daños a edificios.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 3 m/s.</p>"+
"<p>El nivel máximo de profundidad de inundación fue mayor a 1.65 metros.</p>";
let villaArg = "<h1>Situación al 2 de abril del 2013</h1>"+
"<p>Esta zona registró en algunos sectores un nivel de severidad Medio.</p>"+
"<p>Esto significa una pérdida de estabilidad de las personas y vehículos.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 2 m/s.</p>"+
"<p>El nivel máximo de profundidad de inundación fue de 0,61 a 1.05 metros.</p>";
let parqueSM = "<h1>Situación al 2 de abril del 2013</h1>"+
"<p>Esta zona registró en algunos sectores un nivel de severidad Elevada.</p>"+
"<p>Esto significa un riesgo alto para las personas en el exterior.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 3 m/s.</p>"+
"<p>El nivel máximo de profundidad de inundación fue de 1.06 a 1.65 metros.</p>";
let sanCarlos = "<h1>Situación al 2 de abril del 2013</h1>"+
"<p>Esta zona registró en algunos sectores un nivel de severidad Extrema.</p>"+
"<p>Esto significa que puede llegar a presentarse daños a edificios.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 3 m/s.</p>"+
"<p>El nivel máximo de profundidad de inundación fue mayor a 1.65 metros.</p>";
let ciudad = "<h1>Situación al 2 de abril del 2013</h1>"+
"<p>La ciudad de La Plata registró en algunos sectores un nivel de severidad Extrema.</p>"+
"<p>Esto significa que puede llegar a presentarse daños a edificios.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 3 m/s.</p>"+
"<p>El nivel máximo de profundidad de inundación fue mayor a 1.65 metros.</p>";
let cementerio = "<h1>Situación al 2 de abril del 2013</h1>"+
"<p>Esta zona registró en algunos sectores un nivel de severidad Extrema.</p>"+
"<p>Esto significa que puede llegar a presentarse daños a edificios.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 3 m/s.</p>"+
"<p>El nivel máximo de profundidad de inundación fue mayor a 1.65 metros.</p>";


export {barrioJardin,villaElvira,altosDeSanLorenzo,parqueCasteli,LosHornos,Tolosa,estadioMaradona,barrioNorte,Ringuelet,villaArg,parqueSM, sanCarlos,ciudad,cementerio};
//Informacion en INGLES
let barrioJardin2 = "<h1>Situation as of April 2, 2013</h1>"+
"<p>This area registered a High severity level in some sectors.</p>"+
"<p>This means a high risk for people on the outside.</p>"+
"<p>The speed with which the water moved was 2 meters/second.</p>"+
"<p>The maximum flood depth level was 1.06 to 1.65 meters.</p>";
let villaElvira2 = "<h1>Situation as of April 2, 2013</h1>"+
"<p>This area affected in some sectors an extreme level of severity.</p>"+
"<p>This means that damage to buildings may occur.</p>"+
"<p>The speed with which the water moved was 3 m/s. </p>"+
"<p>The maximum flood depth level was greater than 1.65 meters.</p>";
let altosDeSanLorenzo2 = "<h1>Situation as of April 2, 2013</h1>"+
"<p>This area affected in some sectors an extreme level of severity.</p>"+
"<p>This means that damage to buildings may occur.</p>"+
"<p>The speed with which the water moved was 3 m/s. </p>"+
"<p>The maximum flood depth level was greater than 1.65 meters.</p>";
let parqueCasteli2 = "<h1>Situation as of April 2, 2013</h1>"+
"<p>This area affected in some sectors an extreme level of severity.</p>"+
"<p>This means that damage to buildings may occur.</p>"+
"<p>The speed with which the water moved was 3 m/s. </p>"+
"<p>The maximum flood depth level was 1.06 to 1.65 meters.</p>";
let LosHornos2 = "<h1>Situation as of April 2, 2013</h1>"+
"<p>This area affected in some sectors an extreme level of severity.</p>"+
"<p>This means that damage to buildings may occur.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 3 m/s.</p>"+
"<p>The maximum flood depth level was greater than 1.65 meters.</p>";
let Tolosa2 = LosHornos2;
let estadioMaradona2 = "<h1>Situation as of April 2, 2013</h1>"+
"<p>This area registered a High severity level in some sectors.</p>"+
"<p>This means a high risk for people on the outside.</p>"+
"<p>The speed with which the water moved was 3 m/s. </p>"+
"<p>The maximum flood depth level was greater than 1.65 meters.</p>";
let barrioNorte2 = "<h1>Situation as of April 2, 2013</h1>"+
"<p>This area affected in some sectors an extreme level of severity.</p>"+
"<p>This means that damage to buildings may occur.</p>"+
"<p>The speed with which the water moved was 3 m/s. </p>"+
"<p>The maximum flood depth level was 1.06 to 1.65 meters.</p>";
let Ringuelet2 = "<h1>Situation as of April 2, 2013</h1>"+
"<p>This area affected in some sectors an extreme level of severity.</p>"+
"<p>This means that damage to buildings may occur.</p>"+
"<p>The speed with which the water moved was 3 m/s. </p>"+
"<p>The maximum flood depth level was greater than 1.65 meters.</p>";
let villaArg2 = "<h1>Situation as of April 2, 2013</h1>"+
"<p>Esta zona registró en algunos sectores un nivel de severidad Medio.</p>"+
"<p>Esto significa una pérdida de estabilidad de las personas y vehículos.</p>"+
"<p>La rapidez con la que se desplazó el agua fue de 2 m/s.</p>"+
"<p>El nivel máximo de profundidad de inundación fue de 0,61 a 1.05 metros.</p>";
let parqueSM2 = "<h1>Situation as of April 2, 2013</h1>"+
"<p>This area registered a High severity level in some sectors.</p>"+
"<p>This means a high risk for people on the outside.</p>"+
"<p>The speed with which the water moved was 3 m/s. </p>"+
"<p>The maximum flood depth level was 1.06 to 1.65 meters.</p>";
let sanCarlos2 = "<h1>Situation as of April 2, 2013</h1>"+
"<p>This area affected in some sectors an extreme level of severity.</p>"+
"<p>This means that damage to buildings may occur.</p>"+
"<p>The speed with which the water moved was 3 m/s. </p>"+
"<p>The maximum flood depth level was greater than 1.65 meters.</p>";
let ciudad2 = "<h1>Situation as of April 2, 2013</h1>"+
"<p>La ciudad de La Plata registró en algunos sectores un nivel de severidad Extrema.</p>"+
"<p>This means that damage to buildings may occur.</p>"+
"<p>The speed with which the water moved was 3 m/s. </p>"+
"<p>The maximum flood depth level was greater than 1.65 meters.</p>";
let cementerio2 = "<h1>Situation as of April 2, 2013</h1>"+
"<p>This area affected in some sectors an extreme level of severity.</p>"+
"<p>This means that damage to buildings may occur.</p>"+
"<p>The speed with which the water moved was 3 m/s. </p>"+
"<p>The maximum flood depth level was greater than 1.65 meters.</p>";
/*
Situación al 2 de abril del 2013. 
Esta zona registró en algunos sectores un nivel de severidad Leve.
Esto significa una dificultad para caminar.
El caudal de precipitaciones maximo registrado es de ... /La rápidez con la que se desplazó el agua fue de..
El nivel maximo de profundidad de inundación fue de 0.80 a 1.20 metros.
*/

export {barrioJardin2,villaElvira2,altosDeSanLorenzo2,parqueCasteli2,LosHornos2,Tolosa2,estadioMaradona2,barrioNorte2,Ringuelet2,villaArg2,parqueSM2, sanCarlos2,ciudad2,cementerio2};
